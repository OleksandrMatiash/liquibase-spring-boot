package com.vortex.boot.controller;

import com.vortex.boot.model.TestModel;
import com.vortex.boot.service.PostgresqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Autowired
    private PostgresqlService postgresqlService;

    @GetMapping("/postgresql")
    public Iterable<TestModel> postgresql() {
        return postgresqlService.getTestRecords();
    }

}
