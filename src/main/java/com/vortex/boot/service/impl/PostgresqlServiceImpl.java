package com.vortex.boot.service.impl;

import com.vortex.boot.dao.TestRepository;
import com.vortex.boot.model.TestModel;
import com.vortex.boot.service.PostgresqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class PostgresqlServiceImpl implements PostgresqlService {
    @Autowired
    private TestRepository testRepository;

    @PostConstruct
    private void init() {
        getTestRecords();
    }

    @Override
    public Iterable<TestModel> getTestRecords() {
        Iterable<TestModel> all = testRepository.findAll();
        System.out.println("/////  test records: ");
        all.iterator().forEachRemaining(System.out::println);
        return all;
    }
}
