package com.vortex.boot.service;

import com.vortex.boot.model.TestModel;

public interface PostgresqlService {

    Iterable<TestModel> getTestRecords();

}
